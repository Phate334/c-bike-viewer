package tw.phate.cbikeviewer.entities;

import lombok.Data;

@Data
public class Station {

  private String no;
  private String name;
  private String address;
  private String available;
  private String grid;

  public void mappingAndSet(String xmlTag, String text) {
    if(xmlTag.equals("StationNO")) {
      setNo(text);
    } else if(xmlTag.equals("StationName")) {
      setName(text);
    } else if(xmlTag.equals("StationAddress")) {
      setAddress(text);
    } else if(xmlTag.equals("StationNums1")) {
      setAvailable(text);
    } else if(xmlTag.equals("StationNums2")) {
      setGrid(text);
    }
  }
}
