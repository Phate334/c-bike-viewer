package tw.phate.cbikeviewer.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import java.util.List;
import tw.phate.cbikeviewer.R;
import tw.phate.cbikeviewer.adapters.StationsAdapter.ViewHolder;
import tw.phate.cbikeviewer.entities.Station;

public class StationsAdapter extends RecyclerView.Adapter<ViewHolder> {

  private List<Station> stationList;

  class ViewHolder extends RecyclerView.ViewHolder {

    TextView title, available, grid;

    ViewHolder(View itemView) {
      super(itemView);
      this.title = itemView.findViewById(R.id.station_title);
      this.available = itemView.findViewById(R.id.available_value);
      this.grid = itemView.findViewById(R.id.grid_value);
    }
  }

  public StationsAdapter(List<Station> stationList) {
    this.stationList = stationList;
  }

  @NonNull
  @Override
  public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
    View cardView = LayoutInflater.from(parent.getContext())
        .inflate(R.layout.station_card, parent, false);

    return new ViewHolder(cardView);
  }

  @Override
  public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
    Station station = stationList.get(position);
    holder.title.setText(station.getName());
    holder.available.setText(station.getAvailable());
    holder.grid.setText(station.getGrid());
  }

  @Override
  public int getItemCount() {
    return stationList.size();
  }

  public List<Station> getList() {
    return this.stationList;
  }
}