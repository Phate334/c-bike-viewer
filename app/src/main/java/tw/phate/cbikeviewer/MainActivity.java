package tw.phate.cbikeviewer;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import com.android.volley.Cache;
import com.android.volley.Network;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response.Listener;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import java.util.ArrayList;
import java.util.List;
import org.xmlpull.v1.XmlPullParser;
import tw.phate.cbikeviewer.adapters.StationsAdapter;
import tw.phate.cbikeviewer.entities.Station;
import tw.phate.cbikeviewer.network.UpdateAllListener;
import tw.phate.cbikeviewer.network.XmlPullParserRequest;

public class MainActivity extends AppCompatActivity {

  private List<Station> stationList = new ArrayList<>();
  private StationsAdapter adapter = new StationsAdapter(stationList);
  private RecyclerView recyclerView;

  private RequestQueue requestQueue;
  private final static String REQ_TAG = "C-BIKE-DATA-REQUEST";

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);
    setView();

    Cache cache = new DiskBasedCache(getApplicationContext().getCacheDir(), 1024 * 1024);
    Network network = new BasicNetwork((new HurlStack()));
    requestQueue = new RequestQueue(cache, network);
  }

  private void setView() {
    recyclerView = findViewById(R.id.station_cards);
    recyclerView.setAdapter(adapter);
    recyclerView.setLayoutManager(new LinearLayoutManager(this));

    Toolbar toolbar = findViewById(R.id.toolbar);
    setSupportActionBar(toolbar);

    FloatingActionButton fab = findViewById(R.id.add_station);
    fab.setOnClickListener(
        view -> Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
            .setAction("Action", null).show());
  }

  @Override
  protected void onStart() {
    super.onStart();
    requestQueue.start();
    String url = getString(R.string.cbike_data_url);
    Listener<XmlPullParser> listener = new UpdateAllListener(adapter);
    Request<XmlPullParser> request = new XmlPullParserRequest(url, listener, error -> {});
    request.setTag(REQ_TAG);
    requestQueue.add(request);
  }

  @Override
  protected void onStop() {
    super.onStop();
    requestQueue.cancelAll(REQ_TAG);
    requestQueue.stop();
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    // Inflate the menu; this adds items to the action bar if it is present.
    getMenuInflater().inflate(R.menu.menu_main, menu);
    return true;
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    // Handle action bar item clicks here. The action bar will
    // automatically handle clicks on the Home/Up button, so long
    // as you specify a parent activity in AndroidManifest.xml.
    int id = item.getItemId();

    //noinspection SimplifiableIfStatement
    if (id == R.id.action_settings) {
      return true;
    }

    return super.onOptionsItemSelected(item);
  }
}
