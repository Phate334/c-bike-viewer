/**
 * https://github.com/google/volley/blob/master/src/main/java/com/android/volley/toolbox/StringRequest.java
 */
package tw.phate.cbikeviewer.network;

import android.util.Xml;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.toolbox.HttpHeaderParser;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

public class XmlPullParserRequest extends Request<XmlPullParser> {

  private final Object mLock = new Object();

  private Listener<XmlPullParser> mListener;

  public XmlPullParserRequest(String url, Listener<XmlPullParser> listener,
      ErrorListener errorListener) {
    super(Method.GET, url, errorListener);
    mListener = listener;
  }

  @Override
  public void cancel() {
    super.cancel();
    synchronized (mLock) {
      mListener = null;
    }
  }

  @Override
  protected void deliverResponse(XmlPullParser response) {
    Response.Listener<XmlPullParser> listener;
    synchronized (mLock) {
      listener = mListener;
    }
    if (listener != null) {
      listener.onResponse(response);
    }
  }

  @Override
  protected Response<XmlPullParser> parseNetworkResponse(NetworkResponse response) {
    try {
      InputStream dataStream = new ByteArrayInputStream(response.data);
      XmlPullParser parser = Xml.newPullParser();
      parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
      parser.setInput(dataStream, null);
      return Response.success(parser,
          HttpHeaderParser.parseCacheHeaders(response));
    } catch (XmlPullParserException e) {
      return Response.error(new ParseError(e));
    }
  }
}
