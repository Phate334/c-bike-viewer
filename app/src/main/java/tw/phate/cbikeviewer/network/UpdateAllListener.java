package tw.phate.cbikeviewer.network;

import android.util.Xml;
import com.android.volley.Response;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import tw.phate.cbikeviewer.adapters.StationsAdapter;
import tw.phate.cbikeviewer.entities.Station;

public class UpdateAllListener implements Response.Listener<XmlPullParser> {

  private StationsAdapter adapter;
  private List<Station> stations;

  public UpdateAllListener(StationsAdapter adapter) {
    this.adapter = adapter;
    this.stations = adapter.getList();
  }

  @Override
  public void onResponse(XmlPullParser response) {
    try {
      List<Station> stations = parseXML(response);
      this.stations.addAll(stations);
      adapter.notifyDataSetChanged();
    } catch (XmlPullParserException | IOException e) {
      e.printStackTrace();
    }
  }

  private List<Station> parseXML(XmlPullParser parser) throws XmlPullParserException, IOException {
    List<Station> stations = new ArrayList<>();
    int eventType = parser.getEventType();
    Station station = null;
    while (eventType != XmlPullParser.END_DOCUMENT) {
      String tagName = parser.getName();
      if (tagName != null && !"BIKEStationData".equals(tagName) && !"BIKEStation".equals(tagName)) {
        switch (eventType) {
          case XmlPullParser.START_TAG:
            if (tagName.equals("Station")) {
              station = new Station();
            } else if (station != null) {
              station.mappingAndSet(tagName, parser.nextText());
            }
            break;
          case XmlPullParser.END_TAG:
            if (tagName.equals("Station") && station != null) {
              stations.add(station);
              station = null;
            }
            break;
        }
      }
      eventType = parser.next();
    }
    return stations;
  }
}
